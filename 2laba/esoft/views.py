from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from .serializers import ClientSerializers, AgentSerializers, ObjectSerializers, OfferSerializers, NeedSerializers, DealSerializers
from .models import Client, Agent, Object, Offer, Need, Deal

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializers

class AgentViewSet(viewsets.ModelViewSet):
    queryset = Agent.objects.all()
    serializer_class = AgentSerializers

class ObjectViewSet(viewsets.ModelViewSet):
    queryset = Object.objects.all()
    serializer_class = ObjectSerializers
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['types', 'address_street', 'address_city', 'address_house', 'address_number']

class OfferViewSet(viewsets.ModelViewSet):
    queryset = Offer.objects.all()
    serializer_class = OfferSerializers

class NeedViewSet(viewsets.ModelViewSet):
    queryset = Need.objects.all()
    serializer_class = NeedSerializers

class DealViewSet(viewsets.ModelViewSet):
    queryset = Deal.objects.all()
    serializer_class = DealSerializers



